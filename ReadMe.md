# Francis Jude B. Dato - DevOps Engineering Exam

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Task 1.) CPU Usage tracker for local processes

This web application tracks the running foreground and background processes of a machine and render graph based on the number of threads per daemons:

#### Utilized Tools and Library
* [Docker] - is a lightweight, standalone, executable package of software that includes everything needed to run an application.
* [Flask] - is a lightweight WSGI web application framework.
* [psutil] - library for retrieving information onrunning processes and hardware utilization.
* [uwsgi] -  Python WSGI server implementation typically used for running Python web applications.

### Running the Application using Docker
Install docker dependencies then pull the image to default docker registry (DockerHub).

```sh
$ docker pull 77francisjudedato/cpu_usage_tracker:v1
$ docker run -d -p 8000:8000 --rm 77francisjudedato/cpu_usage_tracker:v1
```

For **windows users**, add port mapping ***8000 host*** and ***8000 guest*** to your ***virtualbox advance network settings***

Then Check if the container is launched...
```sh
$ docker ps
```
You should see the following results
![Docker Ps](https://upload.wikimedia.org/wikipedia/commons/1/13/Docker_ps.png)

Then go to your browser's address bar then visit (http://127.0.0.1:8000)...
You should see the same result below.
![Dockerized](https://upload.wikimedia.org/wikipedia/commons/4/4e/Rendered_graph_after_build.png)

### Running the Application using Virtual Environment

```sh
$ git clone https://gitlab.com/FrancisJude/cpu_usage_tracker_flask.git
$ cd cpu_usage_tracker_flask
$ virtualenv env
$ source enb/bin/activate
$ pip install -r requirements.txt
$ python app.py
```
After running, visit (http://127.0.0.1:8001)
You should see the following results..

**Processes in my Windows Computer**
![Virtualenv](https://upload.wikimedia.org/wikipedia/commons/f/f7/Rendered_graph_3.png)


### Task 2.) The application should pull data from ‘https://api.stackexchange.com/2.2/search?intitle=grafana&site=stackoverflow’ and display html links for top 10 topics with the highest view_count.

If the docker image is already existing and the dependencies are already installed, run the ffg commands:
**Using Docker**
```sh
$ docker pull 77francisjudedato/cpu_usage_tracker:v1
$ docker run -d -p 8000:8000 --rm 77francisjudedato/cpu_usage_tracker:v1
```

**Using Virtualenv**
```sh
$ git clone https://gitlab.com/FrancisJude/cpu_usage_tracker_flask.git
$ cd cpu_usage_tracker_flask
$ virtualenv env
$ source enb/bin/activate
$ pip install -r requirements.txt
$ python app.py
```

Then go to your browser's address bar then visit (http://127.0.0.1:8001/articles)...
You should see the same result below.

![Virtualenv](https://upload.wikimedia.org/wikipedia/commons/0/08/Sorted_articles.png)
