from flask import Flask, Markup, render_template
import psutil, os, csv, platform
import requests, json
URL = 'https://api.stackexchange.com/2.2/search?intitle=grafana&site=stackoverflow'
app = Flask(__name__)

@app.route('/')
def render_graph():
  labels =[]
  values =[]
  process = {}
  for id in psutil.pids():
    try:
      labels.append(psutil.Process(id).name())
      values.append(psutil.Process(id).num_threads())
    except Exception as e:
      pass
  print(values)
  write_to_file(labels)
  return render_template('chart.html', title='CPU usage (threads) for the local processes of this machine', max=160, labels=labels, values=values)

@app.route('/articles')
def sort_questions():
   links = requests.get(URL).json().get('items')
   top_views = sorted(links, key = lambda article: article['view_count'], reverse = True)[:9]
   labels = [item['link'] for item in top_views]
   values = [item['view_count'] for item in top_views]
   return render_template('chart.html', title='Top 10 topics with the highest view count', max=800, labels=labels, values=values)

def write_to_file(data):
   active_os = platform.platform()
   delimeter = '\\' if 'windows' in active_os.lower() else '/'
   with open(os.getcwd() + delimeter + "config" + delimeter + "config.csv", 'a') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerow(data)
    csvFile.close()
if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=5001, debug=True)
     app.run(port=8001)